# Ariane SDK for the MEEP project

-------------------------------------------------------------
------------- WARNING STRIPPED DOWN VERSION  ----------------
-------------------------------------------------------------

This repository contains the tools needed to build a bootable Linux-based image for Ariane cores.

This repository is based on [the official Ariane
SDK](https://github.com/pulp-platform/ariane-sdk) (commit 1ee3c61)

## Build a bootable Image

To build an image, you first need to build a rootfs (which requires root
permissions) and then either build an image for Ariane or QEMU,

### Build a rootfs

First build a rootfs for the image. This command needs to be run as root to
manipulate files containted in the rootfs with root permissions.

Support for [qemu usermode emulation for
RISC-V](https://qemu.readthedocs.io/en/latest/user/main.html) and binfmt are
needed to perform configuration steps on the downloaded rootfs.

```
sudo make prepare_fedora_rootfs

```

The rootfs can be found under `build/rootfs/rootfs.cpio.gz`

### Build an Image for Ariane

Compile an image suitable for Ariane. There are two options: OpenSBI or BBL. If
in doubt, build the OpenSBI image.

```
make osbi.bin # osbi flavor (prefered)
```
or
```
make bbl.bin  # bbl flavor
```

The generated image can be found at `./osbi.bin` for OpenSBI or `./bbl.bin` for
BBL. This image is ready to be booted in Ariane; the rootfs is embedded within it.

### Build an Image for QEMU

Compile an image suitable for QEMU (no BBL, no OpenSBI)

```
make Image
```

The generated image can be found at `./Image`. To boot it with QEMU, you can use:

```
 # No network
 qemu-system-riscv64 -nographic -machine virt -m 3G \
     -kernel ./Image \
     -append "rdinit=/sbin/init"
```

To exit qemu, use `ctrl-A X`

## TODO

 - Finish porting U-Boot
