#!/bin/bash

if [ $# -eq 2 ]; then
	rootfs_builddir=$1
	cmd=$2
elif [ $# -eq 1 ]; then
	rootfs_builddir=$1
else
	>&2 echo "Usage run_chroot.sh <root_path> [command_path]"
	exit 1
fi


mount -t proc  /proc ${rootfs_builddir}/proc/
mount -t sysfs /sys  ${rootfs_builddir}/sys/
mount -o bind  /dev  ${rootfs_builddir}/dev/
cp /etc/resolv.conf  ${rootfs_builddir}/etc/resolv.conf

chroot ${rootfs_builddir} /bin/bash $cmd

rm ${rootfs_builddir}/etc/resolv.conf
umount ${rootfs_builddir}/dev
umount ${rootfs_builddir}/sys
umount ${rootfs_builddir}/proc
