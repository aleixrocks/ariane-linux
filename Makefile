ROOT     := $(patsubst %/,%, $(dir $(abspath $(lastword $(MAKEFILE_LIST)))))
RISCV    ?= $(PWD)/install
DEST     := $(abspath $(RISCV))
PATH     := $(DEST)/bin:$(PATH)

NR_CORES := $(shell nproc)


# default configure flags
gnu-toolchain-co-fast = --prefix=$(RISCV) --disable-gdb# no multilib for fast

# default make flags
gnu-toolchain-libc-mk = linux -j$(NR_CORES)

# components paths
configs_path     := $(abspath configs)
linux_defconfig  := $(configs_path)/linux_defconfig
uboot_defconfig  := $(configs_path)/u-boot_defconfig
riscvpk_src_path := $(abspath riscv-pk)
bbl_build_path   := $(abspath build/bbl)
osbi_build_path  := $(abspath build/opensbi)
uboot_build_path := $(abspath build/u-boot)
linux_home_path  := $(abspath build/linux)
linux_build_path := $(linux_home_path)/obj
rootfs_home_path := $(abspath build/rootfs)
rootfs_root_path := $(rootfs_home_path)/rootfs

###########################################################################
## Toolchain targets
###########################################################################

install-dir:
	mkdir -p $(RISCV)

gnu-toolchain-newlib: install-dir
	mkdir -p riscv-gnu-toolchain/build
	cd riscv-gnu-toolchain/build;\
        ../configure --prefix=$(RISCV);\
        cd $(ROOT)

$(RISCV)/bin/riscv64-unknown-elf-gcc: gnu-toolchain-newlib
	cd riscv-gnu-toolchain/build;\
        make -j$(NR_CORES);\
        cd $(ROOT)

gnu-toolchain-no-multilib: install-dir
	mkdir -p riscv-gnu-toolchain/build
	cd riscv-gnu-toolchain/build;\
	../configure $(gnu-toolchain-co-fast);\
	cd $(ROOT)

$(RISCV)/bin/riscv64-unknown-linux-gnu-gcc: gnu-toolchain-no-multilib
	cd riscv-gnu-toolchain/build;\
	make $(gnu-toolchain-libc-mk);\
	cd $(ROOT)

###########################################################################
## Rootfs targets
###########################################################################

.PHONY: prepare_fedora_rootfs
prepare_fedora_rootfs:
	rm -rf $(rootfs_root_path)
	rm -f $(rootfs_home_path)/fedoraimage.raw
	mkdir -p $(rootfs_home_path) $(rootfs_root_path) $(rootfs_home_path)/tmp
	chmod o+rw build
	chmod o+rw $(rootfs_home_path)

	# Download fedora image
	wget -O $(rootfs_home_path)/fedoraimage.raw.xz https://dl.fedoraproject.org/pub/alt/risc-v/repo/virt-builder-images/images/Fedora-Minimal-Rawhide-20200108.n.0-sda.raw.xz
	unxz --keep $(rootfs_home_path)/fedoraimage.raw.xz

	# Extract rootfs from image
	# TODO can we mount a specific partition without using the offset?
	mount -t ext4 -o offset=744488960,rw $(rootfs_home_path)/fedoraimage.raw $(rootfs_home_path)/tmp
	rsync -avz $(rootfs_home_path)/tmp/ $(rootfs_root_path)/
	umount $(rootfs_home_path)/tmp
	rmdir $(rootfs_home_path)/tmp
	rm -f $(rootfs_home_path)/fedoraimage.raw

	cp configs/rootfs_setup_fedora.sh $(rootfs_root_path)/
	tools/run_chroot.sh $(rootfs_root_path) /rootfs_setup_fedora.sh
	rm $(rootfs_root_path)/rootfs_setup_fedora.sh

	cd $(rootfs_root_path) && find . | cpio -H newc -o > $(rootfs_home_path)/rootfs.cpio
	chmod o+rw $(rootfs_home_path)/rootfs.cpio
	cat $(rootfs_home_path)/rootfs.cpio | gzip > $(rootfs_home_path)/rootfs.cpio.gz
	chmod o+rw $(rootfs_home_path)/rootfs.cpio.gz

$(rootfs_home_path)/rootfs.cpio.gz:
	if [ -f "$@" ]; then \
		echo "Please generate a rootfs first. See README.md for more details"; \
		exit 1; \
	else \
		touch $@; \
	fi

###########################################################################
## U-Boot targets
###########################################################################

# ruben config
#$(uboot_build_path)/.config: $(uboot_defconfig)
#	mkdir -p $(uboot_build_path)
#	$(MAKE) -C u-boot/ O=$(uboot_build_path) \
#		CROSS_COMPILE=$(RISCV)/bin/riscv64-unknown-linux-gnu- \
#		meep_ariane_defconfig

$(uboot_build_path)/.config: $(uboot_defconfig)
	mkdir -p $(uboot_build_path)
	cp $(uboot_defconfig) $@
	$(MAKE) -C u-boot/ O=$(uboot_build_path) \
		CROSS_COMPILE=$(RISCV)/bin/riscv64-unknown-linux-gnu- \
		olddefconfig

$(uboot_build_path)/u-boot.bin: $(uboot_build_path)/.config $(RISCV)/bin/riscv64-unknown-elf-gcc
	$(MAKE) -C u-boot/ O=$(uboot_build_path) \
		CROSS_COMPILE=$(RISCV)/bin/riscv64-unknown-linux-gnu- \
		u-boot.bin

u-boot.bin: $(uboot_build_path)/u-boot.bin
	cp $< $@

$(uboot_build_path)/u-boot: $(uboot_build_path)/.config $(RISCV)/bin/riscv64-unknown-elf-gcc
	$(MAKE) -C u-boot/ O=$(uboot_build_path) \
		CROSS_COMPILE=$(RISCV)/bin/riscv64-unknown-linux-gnu- \
		u-boot.bin

u-boot.elf: $(uboot_build_path)/u-boot
	cp $< $@

###########################################################################
## Linux kernel targets
###########################################################################

$(linux_build_path)/.config: $(linux_defconfig) $(RISCV)/bin/riscv64-unknown-linux-gnu-gcc
	mkdir -p $(linux_build_path)
	cp $(linux_defconfig) $@
	$(MAKE) -j $(NR_CORES) -C linux/ O=$(linux_build_path) \
		ARCH=riscv \
		CROSS_COMPILE=$(RISCV)/bin/riscv64-unknown-linux-gnu- \
		olddefconfig

$(linux_build_path)/vmlinux: $(linux_build_path)/.config $(rootfs_home_path)/rootfs.cpio.gz $(RISCV)/bin/riscv64-unknown-linux-gnu-gcc
	MEEP_ROOTFS=$(rootfs_home_path) $(MAKE) -j $(NR_CORES) -C linux/ O=$(linux_build_path) \
		ARCH=riscv \
		CROSS_COMPILE=$(RISCV)/bin/riscv64-unknown-linux-gnu- \
		vmlinux

$(linux_build_path)/arch/riscv/boot/Image: $(linux_build_path)/.config $(rootfs_home_path)/rootfs.cpio.gz $(RISCV)/bin/riscv64-unknown-linux-gnu-gcc
	MEEP_ROOTFS=$(rootfs_home_path) $(MAKE) -j $(NR_CORES) -C linux/ O=$(linux_build_path) \
		ARCH=riscv \
		CROSS_COMPILE=$(RISCV)/bin/riscv64-unknown-linux-gnu- \
		Image

kernel.sym: $(linux_build_path)/vmlinux
	riscv64-unknown-elf-objcopy --only-keep-debug $< $@

Image: $(linux_build_path)/arch/riscv/boot/Image
	cp $< $@

###########################################################################
## BBL targets
###########################################################################

$(bbl_build_path)/bbl: $(RISCV)/bin/riscv64-unknown-elf-gcc $(linux_build_path)/vmlinux
	mkdir -p $(bbl_build_path)
	cd $(bbl_build_path) && $(riscvpk_src_path)/configure --host=riscv64-unknown-elf CC=riscv64-unknown-linux-gnu-gcc OBJDUMP=riscv64-unknown-linux-gnu-objdump --with-payload=$(linux_build_path)/vmlinux --enable-logo --with-logo=$(configs_path)/logo.txt
	make -C $(bbl_build_path)

bbl.bin: $(bbl_build_path)/bbl
	riscv64-unknown-elf-objcopy -S -O binary --change-addresses -0x80000000 $< $@

$(bbl_build_path)/bbl.u-boot: $(RISCV)/bin/riscv64-unknown-elf-gcc $(uboot_build_path)/u-boot.bin
	mkdir -p $(bbl_build_path)
	cd $(bbl_build_path) && $(riscvpk_src_path)/configure --host=riscv64-unknown-elf CC=riscv64-unknown-linux-gnu-gcc OBJDUMP=riscv64-unknown-linux-gnu-objdump --with-payload=$(uboot_build_path)/u-boot.bin --enable-logo --with-logo=$(configs_path)/logo.txt
	make -C $(bbl_build_path)
	mv $(bbl_build_path)/bbl $@

bbl.u-boot.bin: $(bbl_build_path)/bbl.u-boot
	riscv64-unknown-elf-objcopy -S -O binary --change-addresses -0x80000000 $< $@

###########################################################################
## OpenSBI targets
###########################################################################

$(osbi_build_path)/platform/fpga/openpiton/firmware/fw_payload.bin: $(linux_build_path)/arch/riscv/boot/Image $(RISCV)/bin/riscv64-unknown-elf-gcc 
	mkdir -p $(osbi_build_path)
	$(MAKE) -C opensbi/ O=$(osbi_build_path) \
		CROSS_COMPILE=$(RISCV)/bin/riscv64-unknown-elf- \
		PLATFORM=fpga/openpiton \
		FW_PAYLOAD_PATH=$(linux_build_path)/arch/riscv/boot/Image

osbi.bin: $(osbi_build_path)/platform/fpga/openpiton/firmware/fw_payload.bin
	cp $< $@

$(osbi_build_path)/platform/fpga/openpiton/firmware/fw_payload.u-boot.bin: $(uboot_build_path)/u-boot.bin $(RISCV)/bin/riscv64-unknown-elf-gcc 
	mkdir -p $(osbi_build_path)
	$(MAKE) -C opensbi/ O=$(osbi_build_path) \
		CROSS_COMPILE=$(RISCV)/bin/riscv64-unknown-elf- \
		PLATFORM=fpga/openpiton \
		FW_PAYLOAD_PATH=$(uboot_build_path)/u-boot.bin
	mv $(osbi_build_path)/platform/fpga/openpiton/firmware/fw_payload{,.u-boot}.bin

osbi.u-boot.bin: $(osbi_build_path)/platform/fpga/openpiton/firmware/fw_payload.u-boot.bin
	cp $< $@

all: bbl.bin

clean:
	rm -rf $(linux_home_path) $(osbi_build_path) $(uboot_build_path) kernel.sym vmlinux Image bbl.bin osbi.bin
