#!/bin/bash

# Update package information
#dnf check-update
#dnf upgrade

# Set up basic networking
#cat >> /etc/sysconfig/network-scripts/ifcfg-eth0 <<EOF
#DEVICE=eth0
#BOOTPROTO=dhcp
#USERCTL=no
#EOF

# Set up root password
echo 'root:root' | /sbin/chpasswd

# Install and configure ntp tools
#dnf install -y vim #openntpd ntpdate
#sed -i 's/^DAEMON_OPTS="/DAEMON_OPTS="-s /' /etc/default/openntpd

# Mount rootfs rw at boot
# TODO use UUID instead of the device path?
echo "/dev/mmcblk0p3       /               ext4            rw,relatime     0 1" > /etc/fstab

# Enable getty on hvc0 console must be run in container (sudo systemd-nspawn -b -M "riscv" -D <rootfs dir>)
#systemctl unmask serial-getty@hvc0.service
#cp /usr/lib/systemd/system/serial-getty@.service /etc/systemd/system/serial-getty@hvc0.service
#ln -s /etc/systemd/system/serial-getty@hvc0.service /etc/systemd/system/getty.target.wants/
#systemctl enable serial-getty@hvc0.service

# Install basic packages
#dnf install -y vim rsync git flex bison bc make automake gcc gcc-c++
dnf install -y vim tmux screen
dnf install -y trace-cmd strace perf

# Remove unneeded packages
dnf remove -y linux-firmware kernel-modules kernel kernel-core
dnf remove -y selinux-policy-targeted geolite2-city
